import {Component, OnInit} from '@angular/core';
import {ContactService} from '../contact.service';
import {Contact} from '../contact';
import {Router} from '@angular/router';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  protected displayedColumns: String[];

  constructor(protected _cS: ContactService, private _router: Router) {
    this.displayedColumns = ['id', 'name', 'phone', 'email'];
  }

  ngOnInit() {
  }

  goToContact(event: Contact) {
    this._router.navigate(['/contact', event.id]);
  }

}
