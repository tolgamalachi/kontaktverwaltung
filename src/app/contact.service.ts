import {Injectable} from '@angular/core';
import {CONTACTS} from '../assets/contacts';
import {Contact} from './contact';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {DataSource, CollectionViewer} from '@angular/cdk/collections';


@Injectable()
export class ContactService extends DataSource<Contact> {
  private contactsSubject: BehaviorSubject<Contact[]> = new BehaviorSubject([]);
  private contacts: Contact[] = [];

  constructor() {
    super();
    CONTACTS.forEach((c: any) => {
      this.contacts.push(new Contact(c.name, c.phone, c.email, c._id));
    });

    console.dir(this.contacts);
    this.contactsSubject.next([...this.contacts]);
  }

  connect(collectionViewer: CollectionViewer): Observable<Contact[]> {
    return this.getContacts();
  }

  disconnect(collectionViewer: CollectionViewer): void {
  }

  public getContacts(): Observable<Contact[]> {
    return this.contactsSubject.asObservable();
  }

  public addContact(name: string, phone: string, email: string, id: string) {
    this.contacts.push(new Contact(name, phone, email, id));
    this.contactsSubject.next([...this.contacts]);
  }

  getContact(id: String): Contact {
    return this.contacts.find((c) => {
      return c.id === id;
    });
  }
}
