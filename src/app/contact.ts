export class Contact {
  constructor(name: string, phone: string, email: string, id: string) {
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.id = id;
  }

  private _name: string;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  private _phone: string;

  get phone(): string {
    return this._phone;
  }

  set phone(value: string) {
    this._phone = value;
  }

  private _email: string;

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  private _id: string;

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  private _knows: string[] = [];

  get knows(): string[] {
    return this._knows;
  }

  addToKnows(id: string) {
    this._knows.push(id);
  }
}
