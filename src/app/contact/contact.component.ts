import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {ContactService} from '../contact.service';
import {Contact} from '../contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  protected contact: Contact;

  constructor(private _location: Location, private _activatedRoute: ActivatedRoute, private _cS: ContactService) {
  }

  ngOnInit() {
    this.contact = this._cS.getContact(this._activatedRoute.snapshot.params.contactId);
  }


  goBack(): void {
    this._location.back();
  }
}
