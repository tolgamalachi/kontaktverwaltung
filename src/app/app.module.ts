import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdTableModule, MdCardModule, MdButtonModule, MdIconModule} from '@angular/material';


import {AppComponent} from './app.component';
import {ContactListComponent} from './contact-list/contact-list.component';
import {ContactComponent} from './contact/contact.component';


import {RouterModule} from '@angular/router';
import {ContactService} from './contact.service';


@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MdTableModule,
    MdCardModule,
    MdButtonModule,
    MdIconModule,
    RouterModule.forRoot([
      {
        path: '',
        component: ContactListComponent
      },
      {
        path: 'contact/:contactId',
        component: ContactComponent
      }
    ])
  ],
  providers: [ContactService],
  bootstrap: [AppComponent]
})


export class AppModule {
}
