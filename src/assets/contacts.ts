export const CONTACTS = [
  {
    'name': 'Mufasa Burner',
    'phone': '7137131376713',
    'email': 'mufasa.burner@gmail.com',
    '_id': '18eg1edq9d1231ed',
    'knows': [
      '18eg1edq9d1231easdasd',
      '18eg1asdasdedq9d1231ed',
      '18eg1edasdasdaq9d1231ed'
    ]
  },
  {
    'name': 'Eddi Bongo',
    'phone': '71342341376713',
    'email': 'eddi.bongo@gmail.com',
    '_id': '18eg1edq9d1231easdasd',
    'knows': []
  },
  {
    'name': 'Jono Bacon',
    'phone': '71151376713',
    'email': 'jono.bacon@gmail.com',
    '_id': '18eg1asdasdedq9d1231ed',
    'knows': []
  },
  {
    'name': 'Jonny Miller',
    'phone': '131376713',
    'email': 'jonny.miller@gmail.com',
    '_id': '18eg1edasdasdaq9d1231ed',
    'knows': []
  }
];
